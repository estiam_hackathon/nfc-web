import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import Pusher from 'pusher-js';
import { map, Observable, startWith } from 'rxjs';

export interface User {
  name: string;
  email: string;
  id: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})

export class AppComponent implements OnInit{
  title = 'nfc-web';
  myControl = new FormControl<string | User>('');
  options: User[] = [
    {name: 'John Doe', email: 'john.doe@estiam.com', id: 1},
    {name: 'alex', email: 'alex@estiam.com', id: 2},
    {name: 'jane', email: 'jane@estiam.com', id: 3},
    {name: 'james', email: 'jame@estiam.com', id: 4},
  ];
  filteredOptions?: Observable<User[]>;
  pusher : any;
  channel: any;
  isLoading = false;
  isError = false;
  isLogedIn = false;
  user?:User;

  constructor() { }

  ngOnInit() {
    Pusher.logToConsole = true;
    console.log('App Component');
    // init pusher
    this.pusher = new Pusher('062e799f50a78138d263', {
      cluster: 'eu'
    });

    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => {
        const name = typeof value === 'string' ? value : value?.name;
        return name ? this._filter(name as string) : this.options.slice();
      }),
    );
  }



  displayFn(user: User): string {
    return user && user.name ? user.name : '';
  }

  private _filter(name: string): User[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  connect(){
    this.isLoading = true;
    const selectedValue = this.myControl.value;
    if(selectedValue && typeof selectedValue !== 'string' && selectedValue.id){
      this.channel = this.pusher.subscribe('auth-'+selectedValue.id);
      let response = this.channel.bind('LoggedIn', function(data: any) {
        alert('Welcome back '+ data.user.name);
        // set user & token to local storage
        localStorage.setItem('user', JSON.stringify(data.user));
        localStorage.setItem('token', data.token);
        //this.logout();
      });
      setTimeout(() => {
        this.isLoading = false;
        this.channel.unbind('LoggedIn');
        this.pusher.unsubscribe('auth-'+selectedValue.id);
      }, 10000);
    }

  }

  logout(){
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.isLogedIn = false;
  }



}

